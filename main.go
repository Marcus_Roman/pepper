package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"syscall"

	discord "github.com/bwmarrin/discordgo"
)

var (
	client *discord.Session
	conf   *config
)

type config struct {
	Token  string `json:"token"`
	Prefix string `json:"prefix"`
}

func loadConfig(fileName string) *config {
	var (
		config = &config{}
		err    = json.Unmarshal(readFile(fileName), config)
	)

	if err != nil {
		log.Fatal("Couldn't load configuration file")
	}
	return config
}

func main() {
	conf = loadConfig("config/config.json")

	var error error
	client, error = discord.New("Bot " + conf.Token)
	if error != nil {
		log.Fatal("There was an error injecting\n" + error.Error())
		return
	}

	error = client.Open()
	if error != nil {
		log.Fatal("Token is invalid\n" + error.Error())
		return
	}

	loadCommands()

	client.Open()
	client.AddHandler(messageCreate)

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	client.Close()
}
