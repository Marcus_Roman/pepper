package main

import (
	"io/ioutil"
	"log"
	"os"

	discord "github.com/bwmarrin/discordgo"
)

//Ris Return an appended string array
func ris(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}

//Msg send a message
func msg(channel *discord.Channel, message string) {
	client.ChannelMessageSend(channel.ID, message)
}

//Edit edit a message
func edit(channel *discord.Channel, message *discord.Message, str string) {
	client.ChannelMessageEdit(channel.ID, message.ID, str)
}

//ReadFile return the bytes from a file
func readFile(fileName string) []byte {
	file, error := os.Open(fileName)
	if error != nil {
		log.Fatal(error)
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	return b
}
