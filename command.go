package main

import (
	"strings"
	"time"

	discord "github.com/bwmarrin/discordgo"
)

var (
	commands = make(map[string]*command)
)

type command struct {
	aliases *[]string
	execute func(channel *discord.Channel, message *discord.Message, args []string)
}

var about = &command{
	aliases: &[]string{"about"},
	execute: func(channel *discord.Channel, message *discord.Message, args []string) {
		embed := NewEmbed().SetTitle("About").
			SetDescription("Pepper is an experimental bot made for multipurpose\nA important feature of Pepper is that it's built off Go\n"+
				"Speed & efficiency matters most to me -pepper dev").AddField("Source Code", "https://bitbucket.org/").MessageEmbed
		client.ChannelMessageSendEmbed(channel.ID, embed)
	},
}

var ping = &command{
	aliases: &[]string{"ping", "lag"},
	execute: func(channel *discord.Channel, message *discord.Message, args []string) {
		now := time.Now()

		mess, error := client.ChannelMessageSend(channel.ID, "Pong!")
		if error != nil {
			return
		}

		milis := time.Since(now).Round(time.Millisecond)
		edit(channel, mess, "Pong! "+milis.String())
	},
}

func loadCommands() {
	cmds := []*command{ping, about}

	for _, v := range cmds {
		for _, alias := range *v.aliases {
			commands[alias] = v
		}
	}
}

func messageCreate(session *discord.Session, message *discord.MessageCreate) {
	if session.State.User.ID == message.Author.ID {
		return
	}

	if message.Author.Bot {
		return
	}

	if !strings.HasPrefix(message.Message.Content, conf.Prefix) {
		return
	}

	var (
		params   = strings.Split(message.Message.Content, " ")
		executed = strings.TrimPrefix(params[0], conf.Prefix)
		cmd      = commands[strings.ToLower(executed)]
	)

	if cmd == nil {
		return
	}

	channel, error := session.Channel(message.ChannelID)
	if error != nil {
		return
	}
	cmd.execute(channel, message.Message, ris(params, 0))
}
